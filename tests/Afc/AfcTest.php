<?php
/**
 * @category   afc
 */


namespace Afc;

use PHPUnit_Framework_TestCase;


/**
 * Class AfcTest
 *
 * @package Afc
 */
class AfcTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function successful()
    {
        $afc = new Afc();

        $afc->addFeature('feature.a');

        $afc->addFeatureToGroup('feature.a', 1);

        $hasAccess = $afc->hasAccess([1, 2, 3], 'feature.a');

        $this->assertTrue($hasAccess);

    }

    /**
     * @test
     *
     * @return void
     */
    public function featureNotForGroup()
    {
        $afc = new Afc();

        $afc->addFeature('feature.a');

        $afc->addFeatureToGroup('feature.a', 1);

        $hasAccess = $afc->hasAccess([14, 5], 'feature.a');

        $this->assertFalse($hasAccess);
    }

    /**
     * @test
     *
     * @return void
     */
    public function invalidFeature()
    {
        $afc = new Afc();

        $afc->addFeature('feature.z');

        $afc->addFeatureToGroup('feature.a', 1);

        $hasAccess = $afc->hasAccess([1], 'feature.a');

        $this->assertFalse($hasAccess);
    }

    public function a()
    {

    }
}

