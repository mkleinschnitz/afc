<?php
/**
 * @category   afc
 */


namespace Afc;


/**
 * Class Afc
 *
 * @package Afc
 */
class Afc
{
    /**
     * @var array
     */
    private $features = [];

    /**
     * @var array
     */
    private $featureGroupMapping = [];

    /**
     * @param $feature
     *
     * @return $this
     */
    public function addFeature($feature)
    {
        $this->features[$feature] = [
            'id' => $feature
        ];

        return $this;
    }

    /**
     * @param string $feature
     * @param int    $groupId
     *
     * @return $this
     */
    public function addFeatureToGroup($feature, $groupId)
    {
        if (isset($this->features[$feature])) {
            $key                             = $this->getFeatureGroupKey($feature, $groupId);
            $this->featureGroupMapping[$key] = true;
        }

        return $this;
    }

    /**
     * @param array  $groupIds
     * @param string $feature
     *
     * @return bool
     */
    public function hasAccess(array $groupIds, $feature)
    {
        foreach ($groupIds as $groupId) {
            $key = $this->getFeatureGroupKey($feature, $groupId);

            $hasAccess = isset($this->featureGroupMapping[$key]);

            if ($hasAccess) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $feature
     * @param int    $groupId
     *
     * @return string
     */
    private function getFeatureGroupKey($feature, $groupId)
    {
        return sprintf('%s___%s', $feature, $groupId);
    }
}